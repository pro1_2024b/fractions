package exams;
import fractions.Fraction;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

public class ExamUtils
{
    public static List<ExamRecord> readOriginalExamFile(Path path)
            throws IOException
    {
        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
        List<ExamRecord> resultList = new ArrayList<>();
        for(String line : lines)
        {
           String[] mujSplit= line.split("[:=;]");
           resultList.add(new ExamRecord(
                   mujSplit[0],
                   Fraction.parse(mujSplit[1])
           ));
        }
        return resultList;
    }

    public static void writeNormalizedExamFile(Path path, List<ExamRecord> records) throws IOException {
        List<String> normalizedLines = new ArrayList<>();

        for(ExamRecord record : records)
        {
            normalizedLines.add(record.getPersonId()+","+record.getScore());
        }

        Files.write(path,normalizedLines);
    }
}
