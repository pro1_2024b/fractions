package fractions;

public class Fraction
{
    private long n;
    private long d;

    public Fraction(long n, long d)
    {
        this.n = n;
        this.d = d;

        if(n==0)
        {
            this.n = 0;
            this.d = 1;
            return;
        }

        if(this.d < 0)
        {
          this.n = -this.n;
          this.d = -this.d;
        }
        long gcd;
        if(this.n<0)
        {
            gcd = NumericUtils.gcd(-this.n, this.d);
        }
        else
        {
            gcd = NumericUtils.gcd(this.n, this.d);
        }

        this.d = this.d / gcd;
        this.n = this.n / gcd;

        // Zajistit aby d nebylo záporné
        // Vykrátit hodnotou gcd
    }

    @Override
    public String toString()
    {
        return String.format("%s / %s", n, d);
    }

    public Fraction add(Fraction x)
    {
        long d = this.d * x.d ;
        long n = this.d * x.n + this.n * x.d ;
        return new Fraction(n,d);
    }

    public static Fraction parse(String string)
    {
        string = string.replace(" ","");
        String[] splitted = string.split("\\+");

        Fraction sum = new Fraction(0,1);
        for(String s : splitted)
        {
            Fraction f;
            if(s.contains("%"))
            {
                String replace = s.replace("%", "");
                long n = Long.parseLong(replace);
                long d = 100;
                f = new Fraction(n,d);
            }
            else
            {
                String[] split = s.split("/");
                long n = Long.parseLong(split[0]);
                long d = Long.parseLong(split[1]);
                f = new Fraction(n,d);
            }
            sum = sum.add(f);
        }
        return sum;
    }
}
