package pro1;

import fractions.Fraction;

import java.util.Random;

class PerformanceTest1
{
    @org.junit.jupiter.api.Test
    void test()
    {
        int count = 5_000_000;
        Fraction[] pole = new Fraction[count];
        Random random = new Random();
        long time0 = System.currentTimeMillis();
        for(int i=0;i<count;i++)
        {
            pole[i]=new Fraction(
                    random.nextLong(),
                    random.nextLong());
        }
        long time1 = System.currentTimeMillis();
        long timeDiff = time1 - time0;
        ///

        double fractionsPerSecond = 1000d * count / timeDiff;
        System.out.println(fractionsPerSecond);
    }
}